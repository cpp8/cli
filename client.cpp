// proj:rename client.cpp
// Copyright 2020 rs                            proj:subs srini rs@toprllc.com
// License: MIT                                proj:subs unknown MIT

#include <iostream>
#include "options/options.hpp"

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

#include "../common/pwdmgr.pb.h"

using namespace std ;
void test_user() {
    pwdmgr::User user ;
    cout << "User Initialized: " << user.IsInitialized() << endl ;
    user.set_name("srini") ;
    user.set_password("password");
    cout << "User " << user.DebugString() << endl ;
    user.SerializePartialToOstream(&cout) ;
    cout << endl ;
    cout << "User cached size " << user.GetCachedSize() << "Space Used " << user.SpaceUsedLong() << endl ;
    const google::protobuf::Descriptor *userdesc = user.descriptor() ;
    cout << "UserDesc Name " << userdesc->name() << " full name " << userdesc->full_name() << endl ;
    cout << "UserDesc DebugString " << userdesc->DebugString() << endl ;
}

int main(int argc, char **argv) {
    Options options("client", "grpc, protobuf client utility template" , "V0.0") ;
    if (options.Analyze(argc, argv)) {
        cout << "Continuing" << endl ;
    } else {
        exit(EXIT_FAILURE) ;
    }
    test_user() ;
}
