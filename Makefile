include Makefile.std
TOPTARGETS := all clean

SUBDIRS := options
EXEC=cli

$(TOPTARGETS): $(SUBDIRS)
.PHONY: $(TOPTARGETS) $(SUBDIRS)

all: $(SUBDIRS) $(OBJECTS) $(OBJLIB) 
	$(CXX) $(OBJECTS) $(LDFLAGS) $(OBJLIB) -o $(EXEC)

$(EXEC): $(OBJLIB)

$(SUBDIRS):
	$(MAKE) -C $(@) $(MAKECMDGOALS)


test:
	-./cli
	-./cli --help
	./cli --verbose
