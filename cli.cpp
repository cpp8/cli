// proj:rename $(project).cpp
// Copyright 2020 srini                            proj:subs srini $(copyright)
// License: unknown                                proj:subs unknown $(license)

#include <iostream>
#include "options/options.hpp"

using namespace std ;

int main(int argc, char **argv) {
    Options options("template", "command line utility template", "V0.0") ; // proj:substitute template $(project)
    if (options.Analyze(argc, argv)) {
        cout << "Continuing" << endl ;
    } else {
        exit(EXIT_FAILURE) ;
    }
}