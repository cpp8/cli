#include <iostream>
#include <boost/program_options.hpp>
using namespace boost ;
namespace po = boost::program_options ;
#include <algorithm>
#include <iterator>
#include <string>
#include <vector>

#include "options.hpp"
#include "revisions.h"

using namespace std ;

template<class T>
ostream& operator<<(ostream& os, const vector<T>& v)
{
    copy(v.begin(), v.end(), ostream_iterator<T>(os, " "));
    return os;
}
std::vector<std::string> Options::supported {"install" , "add" , "update" , "remove" , "list" };
Options::Options(std::string _name, std::string _desc, std::string _version) 
: Verbose(false) , name(_name) , description(_desc) , version(_version) , Command(UNKNOWN)
{

}


bool Options::Analyze(int _argc, char **_argv) {

    po::options_description global("Global options");
    global.add_options()
        ("verbose,v", "set verbose")
        ("version,V" , "show version")
        ("help,h", "produce help message") 
        ("me,m" , po::value<std::string>() , "my username to access the database")  
        ("command", po::value<std::string>(), "command to execute")
        ("context", po::value<std::string>(), "context ie the sitename")
        ("username", po::value<std::string>(), "username for the context")      
        ("subargs", po::value<std::vector<std::string> >(), "Arguments for command");

    po::positional_options_description pos;
    pos.add("command", 1)
        .add("context", 1)
        .add("username" , 1)
        .add("subargs", -1);

    po::variables_map vm;

    po::parsed_options parsed = po::command_line_parser(_argc, _argv).
        options(global).
        positional(pos).
        allow_unregistered().
        run();

    po::store(parsed, vm);

    if (vm.count("verbose")) {
        Verbose=true ;
    }
    if (vm.count("me")) {
       Me = vm["me"].as<std::string>();
    }

    if (!vm.count("command"))
    {
        cerr << "Command required - install|list|add|update|remove" << endl ;
        return false ;
    }
    std::string cmd = vm["command"].as<std::string>();

    std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
    std::vector<std::string>::iterator opt = opts.begin() ;
    if (std::find(std::begin(supported) , std::end(supported), *opt) == std::end(supported)) {
        cerr << "Unrecognized global switch " << *opt << endl ;
        return false ;
    }
    opts.erase(opts.begin()) ;


    if (vm.count("context")) {
        Context = vm["context"].as<std::string>() ;
        cout << "Context set to " << Context << endl ;
    } else {
        cerr << "Need a context specification " << endl ;
        return false ;
    }

    if (vm.count("username")) {
        Username = vm["username"].as<std::string>() ;
        cout << "Username set to " << Username << endl ;
    } else {
        cout << "No username provided. Will use the default username" << endl ;
    }

    if (cmd == "install")
    {
        Command = INSTALL ;
        return InstallCmd(opts) ;
    }
    if (cmd == "list")
    {
        Command = LIST ;
        return ListCmd(opts) ;
    }
    if (cmd == "add")
    {
        Command = ADD ;
        return AddCmd(opts) ;
    }
    if (cmd == "update")
    {
        Command = UPDATE ;
        return UpdateCmd(opts) ;
    }

    if (cmd == "remove")
    {
        Command = REMOVE ;
        return RemoveCmd(opts) ;
    }

    cerr << "Unrecognized command " << endl ;
    return false ;
}


void Options::ShowVersionDetail() const {
    cout << name << " - " << description << " - " << __DATE__ << " " << __TIME__ << endl ;
    cout << "Major: " << VERSION_MAJOR << " Minor : " << VERSION_MINOR << " Build: " << VERSION_BUILD << endl ;
    cout << "Version Recorded: " << BUILD_TIME << endl ;
    cout << "Repo URL: " << REPO_URL << " Branch: " << BRANCH_NAME << " Commit Id: " << SHORT_COMMIT_ID << " Tags: " << ASSIGNED_TAGS << endl ;
}

void Options::Show() const {
    cout << "Inputs are: " << Arguments << endl ;
}

void Options::ShowArgs(std::vector<std::string> _opts) {
    cout << "Arguments " << _opts << endl ;
}

bool Options::InstallCmd(std::vector<std::string> _opts) {
    cout << "Install Command" << _opts << endl ;

    cout << "Server site set to " << Context << endl ;
    cout << "Default username is " << Username << endl ;
    return false ;
}
bool Options::ListCmd(std::vector<std::string> _opts) {
    cout << "List Command" << endl ;
    return false ;
}
bool Options::AddCmd(std::vector<std::string> _opts) {
    cout << "Add Command" << _opts <<  endl ;
    return false ;
}

bool Options::UpdateCmd(std::vector<std::string> _opts) {
    cout << "Update Command" << endl ;
    return false ;
}

bool Options::RemoveCmd(std::vector<std::string> _opts) {
    cout << "Remove Command" << endl ;
    return false ;
}