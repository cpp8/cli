#ifndef _OPTIONS_HPP_
#define _OPTIONS_HPP_
#include <string>
#include <vector>

typedef enum  {
   UNKNOWN ,
   INSTALL ,
   LIST ,
   ADD ,
   UPDATE ,
   REMOVE 
} CommandS ;

class Options {
public:

    CommandS Command ;
    std::string Me ;
    bool Verbose ;
    std::string Context ;
    std::string Server ;
    std::string Username ;
    
    Options(std::string _name, std::string _desc, std::string _version) ;
    bool Analyze(int _argc, char **_argv) ;
    void ShowVersionDetail() const ;
    void Show() const ;

    std::vector< std::string > Arguments ;

private:
    static std::vector<std::string> supported;
    bool InstallCmd(std::vector<std::string> _opts)  ;
    bool ListCmd(std::vector<std::string> _opts) ;
    bool AddCmd(std::vector<std::string> _opts) ;
    bool UpdateCmd(std::vector<std::string> _opts) ;
    bool RemoveCmd(std::vector<std::string> _opts) ;
    void ShowArgs(std::vector<std::string> _opts) ;
    std::string name ;
    std::string description ;
    std::string version ;
    int argc ;
    char **argv ;
    Options() ;
} ;

#endif